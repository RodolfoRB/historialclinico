package com.vivoideas.historialClinico.controllers;

import com.vivoideas.historialClinico.config.MappingController;
import com.vivoideas.historialClinico.core.models.Receta;
import com.vivoideas.historialClinico.core.services.RecetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

/**
 * Created by Infecta on 14/05/2016.
 */
@RestController
@RequestMapping(MappingController.Recetas.CONTROLLER)
public class RecetaController {
    private RecetaService recetaService;

    public RecetaService getRecetaService() {
        return recetaService;
    }
    public RecetaController() {
    }
    @Autowired
    public RecetaController(RecetaService recetaService) {
        this.recetaService = recetaService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Receta> getAll(){
        return getRecetaService().getReceta();
    }
}
