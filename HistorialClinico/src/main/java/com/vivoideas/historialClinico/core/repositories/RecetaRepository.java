package com.vivoideas.historialClinico.core.repositories;

import com.vivoideas.historialClinico.core.models.Receta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;

/**
 * Created by Infecta on 14/05/2016.
 */
public interface RecetaRepository extends JpaRepository<Receta, Long> {
}
