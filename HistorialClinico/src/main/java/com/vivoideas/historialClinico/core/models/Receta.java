package com.vivoideas.historialClinico.core.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Infecta on 14/05/2016.
 */
@Entity
public class Receta {
    @Id
    @GeneratedValue
    private long id;

    private String notas;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }
}
