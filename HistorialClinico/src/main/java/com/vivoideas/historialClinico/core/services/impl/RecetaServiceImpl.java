package com.vivoideas.historialClinico.core.services.impl;

import com.vivoideas.historialClinico.core.models.Receta;
import com.vivoideas.historialClinico.core.repositories.RecetaRepository;
import com.vivoideas.historialClinico.core.services.RecetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by Infecta on 14/05/2016.
 */
@Service
public class RecetaServiceImpl implements RecetaService {
    private RecetaRepository recetaRepository;
    @Override
    public Collection<Receta> getReceta() {
        return getRecetaRepository().findAll();
    }

    public RecetaRepository getRecetaRepository() {
        return recetaRepository;
    }

    @Autowired
    public RecetaServiceImpl(RecetaRepository recetaRepository) {
        this.recetaRepository = recetaRepository;
    }
}
