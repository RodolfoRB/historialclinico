package com.vivoideas.historialClinico.core.services;

import com.vivoideas.historialClinico.core.models.Receta;

import java.util.Collection;

/**
 * Created by Infecta on 14/05/2016.
 */
public interface RecetaService {
    public Collection<Receta> getReceta();
}
